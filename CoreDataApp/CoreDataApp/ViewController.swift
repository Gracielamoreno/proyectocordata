//
//  ViewController.swift
//  CoreDataApp
//
//  Created by Sebas on 24/1/18.
//  Copyright © 2018 Sebas. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var singlePerson: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func savePerson() {
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let person = Person(entity: entityDescription!, insertInto: manageObjectContext)
        
        person.address = addressTextField.text ?? ""
        person.name = nameTextField.text ?? ""
        person.phone = phoneTextField.text ?? ""
        
        do {
            
            try manageObjectContext.save()
            clearFields()
            
        } catch {
            print("Error")
        }
        
    }
    
    func clearFields(){
        
        addressTextField.text = ""
        nameTextField.text = ""
        phoneTextField.text = ""
        
    }

    @IBAction func saveButtonPressed(_ sender: Any) {
        
        savePerson()
        
    }
    
    @IBAction func findButtonPressed(_ sender: Any) {
        
        if nameTextField.text == "" {
            
            findAll()
            return
        }
        
        findOne()
        
        
    }
    
    func findAll(){
        
        //let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            for result in results {
                
                let person = result as! Person
                
                print("Name: \(person.name ?? "")  ", terminator: "")
                print("Address: \(person.address ?? "")  ", terminator: "")
                print("Phone: \(person.phone ?? "")  ", terminator: "")
                print()
                print()
                
            }
            
        } catch {
            print("Error finding all")
        }
    }
    
    func findOne(){
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        request.entity = entityDescription
        
        let predicate = NSPredicate(format: "name = %@", nameTextField.text!)
        
        request.predicate = predicate
        
        do {
            
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            if results.count > 0 {
                let match = results[0] as! Person
                   singlePerson = match
             performSegue(withIdentifier: "FinOneSegue", sender: self)
            } else {
                
                addressTextField.text = "n/a"
                nameTextField.text = "n/a"
                phoneTextField.text = "n/a"
                
            }
            
        } catch {
            print("Error finding one")
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="finOneSegue"{
            let destination = segue.destination as! finOneViewController1
            destination.person = singlePerson         }
    }
    
}














