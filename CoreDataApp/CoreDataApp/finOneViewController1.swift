//
//  finOneViewController1.swift
//  CoreDataApp
//
//  Created by Graciela Moreno on 30/1/18.
//  Copyright © 2018 Sebas. All rights reserved.
//

import UIKit

class finOneViewController1: UITabBarController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    var person:Person?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = person?.name
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Delete(_ sender: Any) {
        let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        manageObjectContext.delete(person!)
        do{
            try manageObjectContext.save()
        }catch{
            print("Error deleting object")
        }
    }
    
    

}
